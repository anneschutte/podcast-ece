<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/sermon")
 */
class SermonController extends Controller
{
    /**
     * @Route("")
     * @Method("GET")
     */
    public function getAction(Request $request)
    {

        $limit = $request->query->getInt('limit', 10);
        $offset = $request->query->getInt('offset', 0);

        $sermons = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Sermon')
            ->findBy([
                    'isDeleted' => false,
                    'isPublished' => true
                ],
                null, /* TODO: apply service date*/
                $limit,
                $offset
            );

        dump($sermons);

        $res = new JsonResponse();
        $res->setContent($this->get('serializer')->serialize($sermons, 'json'));
        return $res;
    }
}
