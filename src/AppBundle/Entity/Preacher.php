<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PreacherRepository")
 * @ORM\Table(name="preacher")
 * @ORM\HasLifecycleCallbacks

 */
class Preacher
{
	public function __construct()
	{
		$this->sermons = new ArrayCollection();
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="firstname", type="string", length=255)
	 */
	private $firstname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="middlename", type="string", length=255, nullable=true)
	 */
	private $middlename;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastname", type="string", length=255)
	 */
	private $lastname;


	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 */
	private $created;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 */
	private $updated;


	/**
	 * @var bool
	 * @ORM\Column(name="is_deleted", type="boolean")
	 */
	private $isDeleted = false;


	/**
	 * @ORM\OneToMany(targetEntity="Sermon", mappedBy="preacher")
	 */
	private $sermons;



	/**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Preacher
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set middlename
     *
     * @param string $middlename
     *
     * @return Preacher
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return string
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Preacher
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Preacher
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Preacher
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return Preacher
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

	/**
	 * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
	 *
	 * @ORM\PrePersist
	 */
	public function updatedTimestamps()
	{
		$now = new \DateTime();
		if($this->getCreated() == null)
		{
			$this->setCreated($now);
		}
		else
		{
			$this->setUpdated($now);
		}
	}

    /**
     * Add sermon
     *
     * @param \AppBundle\Entity\Sermon $sermon
     *
     * @return Preacher
     */
    public function addSermon(\AppBundle\Entity\Sermon $sermon)
    {
        $this->sermons[] = $sermon;

        return $this;
    }

    /**
     * Remove sermon
     *
     * @param \AppBundle\Entity\Sermon $sermon
     */
    public function removeSermon(\AppBundle\Entity\Sermon $sermon)
    {
        $this->sermons->removeElement($sermon);
    }

    /**
     * Get sermons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSermons()
    {
        return $this->sermons;
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return implode(' ', [$this->getFirstname(), $this->getMiddlename(), $this->getLastname()]);
    }
}
