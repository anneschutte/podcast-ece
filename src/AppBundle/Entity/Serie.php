<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Serie
 *
 * @ORM\Table(name="serie")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SerieRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Serie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

	/**
	 * @ORM\OneToMany(targetEntity="Sermon", mappedBy="serie")
	 */
	private $sermons;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Serie
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Serie
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Serie
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Serie
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }


	/**
	 * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
	 *
	 * @ORM\PrePersist
	 */
	public function updatedTimestamps()
	{
		$now = new \DateTime();
		if($this->getCreated() == null)
		{
			$this->setCreated($now);
		}
		else
		{
			$this->setUpdated($now);
		}
	}


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sermons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sermon
     *
     * @param \AppBundle\Entity\Sermon $sermon
     *
     * @return Serie
     */
    public function addSermon(\AppBundle\Entity\Sermon $sermon)
    {
        $this->sermons[] = $sermon;

        return $this;
    }

    /**
     * Remove sermon
     *
     * @param \AppBundle\Entity\Sermon $sermon
     */
    public function removeSermon(\AppBundle\Entity\Sermon $sermon)
    {
        $this->sermons->removeElement($sermon);
    }

    /**
     * Get sermons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSermons()
    {
        return $this->sermons;
    }
}
