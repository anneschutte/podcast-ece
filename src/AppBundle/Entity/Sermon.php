<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SermonRepository")
 * @ORM\Table(name="sermon")
 * @ORM\HasLifecycleCallbacks

 */
class Sermon
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=255)
	 */
	private $title;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 */
	private $created;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 */
	private $updated;

	/**
	 * @var bool
	 * @ORM\Column(name="is_published", type="boolean")
	 */
	private $isPublished = false;

	/**
	 * @var bool
	 * @ORM\Column(name="is_deleted", type="boolean")
	 */
	private $isDeleted = false;

    /**
     * @var string
     * @ORM\Column(name="link", type="string", length=512)
     */
	private $link = null;


//	/**
//	 * @ORM\OneToMany(targetEntity="Content", mappedBy="page", cascade={"persist","remove"})
//	 */
	/**
	 * @ORM\ManyToOne(targetEntity="Preacher", inversedBy="sermons")
	 * @ORM\JoinColumn(name="preacher_id", referencedColumnName="id")
	 */
	protected $preacher;

	/**
	* @ORM\ManyToOne(targetEntity="Serie", inversedBy="sermons")
	* @ORM\JoinColumn(name="serie_id", referencedColumnName="id")
	*/
	protected $serie;

    /**
     * @ORM\ManyToMany(targetEntity="Channel", inversedBy="sermons")
     * @ORM\JoinTable(name="channels_sermons")
     */
    protected $channels;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Sermon
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Sermon
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Sermon
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Sermon
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     *
     * @return Sermon
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return Sermon
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }




	/**
	 * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
	 *
	 * @ORM\PrePersist
	 */
	public function updatedTimestamps()
	{
		$now = new \DateTime();
		if($this->getCreated() == null)
		{
			$this->setCreated($now);
		}
		else
		{
			$this->setUpdated($now);
		}
	}

    /**
     * Set preacher
     *
     * @param \AppBundle\Entity\Preacher $preacher
     *
     * @return Sermon
     */
    public function setPreacher(\AppBundle\Entity\Preacher $preacher = null)
    {
        $this->preacher = $preacher;

        return $this;
    }

    /**
     * Get preacher
     *
     * @return \AppBundle\Entity\Preacher
     */
    public function getPreacher()
    {
        return $this->preacher;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Sermon
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set serie
     *
     * @param \AppBundle\Entity\serie $serie
     *
     * @return Sermon
     */
    public function setSerie(\AppBundle\Entity\serie $serie = null)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get serie
     *
     * @return \AppBundle\Entity\serie
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set channel
     *
     * @param \AppBundle\Entity\Channel $channels
     *
     * @return Sermon
     */
    public function setChannels(\AppBundle\Entity\Channel $channels = null)
    {
        $this->channels = $channels;

        return $this;
    }

    /**
     * Get channel
     *
     * @return \AppBundle\Entity\Channel
     */
    public function getChannels()
    {
        return $this->channels;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->channels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add channel
     *
     * @param \AppBundle\Entity\Channel $channel
     *
     * @return Sermon
     */
    public function addChannel(\AppBundle\Entity\Channel $channel)
    {
        $this->channels[] = $channel;

        return $this;
    }

    /**
     * Remove channel
     *
     * @param \AppBundle\Entity\Channel $channel
     */
    public function removeChannel(\AppBundle\Entity\Channel $channel)
    {
        $this->channels->removeElement($channel);
    }
}
