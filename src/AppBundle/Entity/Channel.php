<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Channel
 *
 * @ORM\Table(name="channel")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ChannelRepository")
 */
class Channel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="Sermon", mappedBy="channels")
     */
    private $sermons;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Channel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Channel
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sermons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sermon
     *
     * @param \AppBundle\Entity\Sermon $sermon
     *
     * @return Channel
     */
    public function addSermon(\AppBundle\Entity\Sermon $sermon)
    {
        $this->sermons[] = $sermon;

        return $this;
    }

    /**
     * Remove sermon
     *
     * @param \AppBundle\Entity\Sermon $sermon
     */
    public function removeSermon(\AppBundle\Entity\Sermon $sermon)
    {
        $this->sermons->removeElement($sermon);
    }

    /**
     * Get sermons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSermons()
    {
        return $this->sermons;
    }
}
