<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Sermon;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SermonType extends AbstractType
{
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Sermon::class,
		]);
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('isPublished', CheckboxType::class)
			->add('title', TextType::class)
			->add('description', TextareaType::class)
			->add('link', TextType::class)
			->add('channels', EntityType::class, [
				'class' => 'AppBundle:Channel',
				'choice_label' => 'name',
				'multiple' => true,
				'expanded' => true
			])
			->add('serie', EntityType::class, [
				'class' => 'AppBundle:Serie',
				'choice_label' => 'title'
			])
			->add('preacher', EntityType::class, [
				'class' => 'AppBundle:Preacher',
				'choice_label' => 'fullname'
			])
		;
	}
}