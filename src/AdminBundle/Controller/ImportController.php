<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Channel;
use AppBundle\Entity\Preacher;
use AppBundle\Entity\Serie;
use AppBundle\Entity\Sermon;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * @Route("/import")
 */
class ImportController extends Controller
{
    /**
     * @Route("/test")
     * @Method("GET")
     */
    public function indexAction()
    {
	    $rootDir = $this->get('kernel')->getRootDir();
    	$rawJson = file_get_contents($rootDir.'\..\_docs\data.json');

//        $rawJson = ""; // TODO: get this from request body
        $json = json_decode($rawJson);

        if (!$this->allPropertiesExists($json, ['services', 'series', 'preachers', 'sermons', 'files'])) {
            throw new \Exception("JSON doc is missing one of these: ['services', 'series', 'preachers', 'sermons', 'files']");
        }

        $manager = $this->getDoctrine()->getManager();

//        echo "<pre>";
        dump("Decoded JSON. \n");

	    // Import preachers
        $preachers = []; // [json_id => preacher, ...]
        foreach ($json->preachers as $preacherRaw)
        {
            $preacher = $this->preacherRawToObj($preacherRaw);
            $manager->persist($preacher);
            $manager->flush(); // Flush now, we want to map it to the id property of its old json object
            $preachers[$preacherRaw->id] = $preacher;

	        dump("Imported preacher " . $preacher->getFullname() . ", id in db: " . $preacher->getId() . ", id in json: " . $preacherRaw->id);
        }

        // Import series
        $series = []; // [json_id => serie, ...]
        foreach ($json->series as $serieRaw) {
            $serie = $this->serieRawToObj($serieRaw);
            $manager->persist($serie);
            $manager->flush(); // Flush now, we want to map it to the id property of its old json object
            $series[$serieRaw->id] = $serie;

            dump( "Imported series item " . $serie->getTitle() . ", id in db: " . $serie->getId() . ", id in json: " . $serieRaw->id);
        }

        // Import file links
        // [json_id => file, ...]
        $files = $this->filesRawToArray($json->files);

        // Import the first channel (service)
        $service = $json->services[0];
        $channel = new Channel();
        $channel->setName($service->name);
        $manager->persist($channel);
        $manager->flush();

        dump( "Imported channel " . $channel->getName() . ", id in db: " . $channel->getId() . ", id in json: " . $service->id);

        // Import sermons
        foreach ($json->sermons as $sermonRaw) {
            $sermon = $this->sermonRawToObj($sermonRaw, $files, $preachers, $series, $channel);
            $manager->persist($sermon);
            $manager->flush();

	        dump( "Imported sermon " . $sermon->getTitle() . ", id in db: " . $sermon->getId() . ", id in json: " . $sermonRaw->id  );
        }

        $manager->flush();

	    dump( "Done"  );
//        echo "</pre>";


	    return $this->render('AdminBundle:Default:index.html.twig');
    }

    private function preacherRawToObj($raw)
    {

        if (empty($raw->name)) {
            throw new Exception("Expected preacher to have a non empty property 'name'");
        }

        $nameExploded = explode(' ', $raw->name);

        $preacher = new Preacher();

        $cnt = count($nameExploded);
        if ($cnt == 2) {
            $preacher->setFirstname($nameExploded[0]);
            $preacher->setLastname($nameExploded[1]);
        } else if ($cnt == 3) {
            $preacher->setFirstname($nameExploded[0]);
            $preacher->setMiddlename($nameExploded[1]);
            $preacher->setLastname($nameExploded[2]);
        } else {
            throw new Exception("Expected preacher to have 2 or 3 name parts (split by space char)");
        }

        return $preacher;
    }

    private function serieRawToObj($raw)
    {
        if (empty($raw->name)) {
            throw new Exception("Expected serie to have a non empty property 'name'");
        }

        $serie = new Serie();
        $serie->setTitle($raw->name);
        return $serie;
    }

    /**
     * @param $raw \StdClass
     * @param $files [] Array of all files, where the key is the file.sermon_id and the value file.name
     * @param $preachers [] Array of all preachers, where the key is 'preacher_id' as it is known in the json doc.
     * @param $series [] Array of series, where the key is the 'serie_id' as it is known in the json doc
     * @param $channel Channel The channel, for now that should be the ECE channel, as all old data is from the ECE services
     * @return Sermon
     */
    private function sermonRawToObj($raw, $files, $preachers, $series, $channel)
    {
        /*
         * Object example:
         * {"id":1,"title":"Jezus...","preacher_id":3,"datetime":"2015-12-06 10:00:00","service_id":5,"series_id":3,"start":"a:0:{}","end":"a:0:{}","description":"","time":"","override":0,"page_id":0}
         */

        $this->allPropertiesExists($raw, ['title', 'preacher_id', 'series_id']);

        $sermon = new Sermon();
        $sermon->setTitle($raw->title);
        $sermon->setPreacher($preachers[$raw->preacher_id]);
        $sermon->setSerie($series[$raw->series_id]);
        $sermon->addChannel($channel);
        $sermon->setIsDeleted(false);
        $sermon->setIsPublished(true);
        $sermon->setLink($files[$raw->id]);
        return $sermon;
    }

    private function filesRawToArray($filesArr)
    {
        /*
         * Item oject example:
         *  { "id": 2, "type": "file", "name": "20151129Willem Plaizier_Laat Uw koninkrijk komen.mp3", "sermon_id": 2, "count": 899, "duration": "37:08"}
         */

        $result = array();
        foreach ($filesArr as $file) {
            $result[$file->sermon_id] = $file->name;
        }

        return $result;
    }

    private function allPropertiesExists($obj, $propertyNames)
    {
        foreach ($propertyNames as $p) {
            if (!property_exists($obj, $p)) {
                throw new \Exception("Missing property '" . $p . "' in object: " . var_export($obj, true));
            }
        }

        return true;
    }
}
