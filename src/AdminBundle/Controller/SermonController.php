<?php

namespace AdminBundle\Controller;

use AdminBundle\Datatable\SermonDatatable;
use AdminBundle\Form\SermonType;
use AppBundle\Entity\Sermon;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/sermon")
 */
class SermonController extends Controller
{
    /**
     * @Route("/list")
     */
    public function listAction(Request $request)
    {
	    $isAjax = $request->isXmlHttpRequest();

	    $datatable = $this->get('sg_datatables.factory')->create(SermonDatatable::class);
	    $datatable->buildDatatable();

	    if ($isAjax) {
		    $responseService = $this->get('sg_datatables.response');
		    $responseService->setDatatable($datatable);

		    $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
		    $datatableQueryBuilder->buildQuery();

		    return $responseService->getResponse();
	    }

        return $this->render('AdminBundle:Sermon:list.html.twig', [
            'datatable' => $datatable
        ]);
    }


    /**
     * @Route("/edit/{id}", name="admin_sermon_edit")
     * @ParamConverter("sermon", class="AppBundle:Sermon")
     */
    public function editAction(Request $request, Sermon $sermon, $isEdit = true)
    {
	    $form = $this->createForm(SermonType::class, $sermon)
         ->add('save', SubmitType::class, [
             'label' => 'Save',
             'attr' => [
                 'class' => 'btn btn-primary'
             ]
         ]);

	    if(!$isEdit)
	    {
		    //  default value for new sermon (add action), published is checked
		    $form->get( 'isPublished' )->setData( true );
	    }

	    $form->handleRequest($request);

	    if ($form->isValid())
	    {
		    $em = $this->getDoctrine()->getManager();
		    $em->persist($sermon);
		    $em->flush();

		    return $this->redirectToRoute('admin_sermon_list');
	    }

	    return $this->render('AdminBundle:Sermon:edit.html.twig', [
		    'form' => $form->createView(),
		    'sermon' => $sermon,
		    'action' => $isEdit ? 'edit' : 'add'
	    ]);

    }

    /**
     * @Route("/add")
     */
    public function addAction(Request $request)
    {   //  create new sermon
        $sermon = new Sermon();

        //  handle in edit action - avoid duplicate code
        return $this->editAction($request, $sermon, false);
    }


	/**
	 * @Route("/download/{id}")
	 * @ParamConverter("sermon", class="AppBundle:Sermon")
	 */
	public function downloadAction(Request $request, Sermon $sermon)
	{
		$file = $this->getParameter('uploads_directory') . $sermon->getLink();

		// check if file exists
		$fs = new FileSystem();
		if(!$fs->exists($file))
		{
			throw $this->createNotFoundException();
		}

		// prepare BinaryFileResponse
		$response = new BinaryFileResponse($file);

//		$response->trustXSendfileTypeHeader();

		$response->setContentDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT
		);

		return $response;
	}
}
