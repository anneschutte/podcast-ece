<?php

namespace AdminBundle\Controller;

use AdminBundle\Datatable\PreacherDatatable;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/preacher")
 */
class PreacherController extends Controller
{
    /**
     * @Route("/list")
     */
    public function listAction(Request $request)
    {
	    $isAjax = $request->isXmlHttpRequest();

	    $datatable = $this->get('sg_datatables.factory')->create(PreacherDatatable::class);
	    $datatable->buildDatatable();

	    if ($isAjax) {
		    $responseService = $this->get('sg_datatables.response');
		    $responseService->setDatatable($datatable);

		    $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
		    $datatableQueryBuilder->buildQuery();

		    return $responseService->getResponse();
	    }

	    return $this->render('AdminBundle:Preacher:list.html.twig', [
		    'datatable' => $datatable
	    ]);
    }

	/**
	 * @Route("/add", name="admin_preacher_add")
	 */
	public function addAction(Request $request)
	{

	}

	/**
	 * @Route("/edit/{id}", name="admin_preacher_edit")
	 */
    public function editAction(Request $request, $id)
    {
	    $preacher = $this->getDoctrine()
         ->getRepository('AppBundle:Preacher')
         ->find($id);


	    $builder = $this->createFormBuilder($preacher)
            ->add('firstname',TextType::class,[
            	'label' => 'Firstname'
            ])
		    ->add('middlename',TextType::class,[
			    'label' => 'middlename'
		    ])
		    ->add('lastname',TextType::class,[
			    'label' => 'Lastname'
		    ])
            ->add('save', SubmitType::class,   [
                'label' => 'Save',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);

	    $form = $builder->getForm();


	    $form->handleRequest($request);

	    if($form->isValid())
	    {

		    $em = $this->getDoctrine()->getManager();

		    $em->persist($preacher);
		    $em->flush();

		    return $this->redirectToRoute('admin_preacher_list');

	    }

	    return $this->render('AdminBundle:Preacher:edit.html.twig', [
		    'preacher' => $preacher,
		    'form' => $form->createView(),
	    ]);

    }
}
