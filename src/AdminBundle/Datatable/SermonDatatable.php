<?php

namespace AdminBundle\Datatable;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class SermonDatatable
 *
 * @package AppBundle\Datatables
 */
class SermonDatatable extends AbstractDatatable
{

	public function getLineFormatter()
	{
		$formatter = function($row) {
			$row['preacher_fullname'] = implode(' ', [$row['preacher']['firstname'], $row['preacher']['middlename'] , $row['preacher']['lastname']]);

			return $row;
		};

		return $formatter;
	}


    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
            //'language' => 'de'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set([
//            'individual_filtering' => true,
//            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'classes' => 'table table-hover table-striped',//Style::BOOTSTRAP_3_STYLE,
        ]);

        $this->features->set(array(
        ));

        $this->columnBuilder
//            ->add('id', Column::class, array(
//                'title' => 'Id',
//                ))
	        ->add('isPublished', BooleanColumn::class, array(
		        'title' => 'Published',
		        'true_icon' => 'glyphicon glyphicon-ok',
		        'false_icon' => 'glyphicon glyphicon-remove',
	        ))
            ->add('title', Column::class, array(
                'title' => 'Title',
                ))


//            ->add('link', Column::class, array(
//                'title' => 'Link',
//                ))
	        ->add('preacher.firstname', Column::class, array(
		        'title' => 'Preacher firstname',
		        'visible' => false
	        ))
	        ->add('preacher.middlename', Column::class, array(
		        'title' => 'Preacher middlename',
		        'visible' => false
	        ))
	        ->add('preacher.lastname', Column::class, array(
		        'title' => 'Preacher lastname',
		        'visible' => false
	        ))

	        ->add('preacher_fullname', VirtualColumn::class, array(
                'title' => 'Preacher',
                'searchable' => true,
                'orderable' => true,
                'order_column' => 'preacher.firstname',
                'search_column' => 'preacher.firstname',
                ))

            ->add('serie.title', Column::class, array(
                'title' => 'Serie',
                ))
            ->add('channels.name', Column::class, array(
                'title' => 'Channel',
                'data' => 'channels[, ].name'
                ))
            ->add(null, ActionColumn::class, array(
                'title' => '',
                'actions' => array(
//                    array(
//                        'route' => 'sermon_show',
//                        'route_parameters' => array(
//                            'id' => 'id'
//                        ),
//                        'label' => $this->translator->trans('sg.datatables.actions.show'),
//                        'icon' => 'glyphicon glyphicon-eye-open',
//                        'attributes' => array(
//                            'rel' => 'tooltip',
//                            'title' => $this->translator->trans('sg.datatables.actions.show'),
//                            'class' => 'btn btn-primary btn-xs',
//                            'role' => 'button'
//                        ),
//                    ),
                    [
                        'route' => 'admin_sermon_edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        'icon' => 'glyphicon glyphicon-pencil',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('Edit sermon'),
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ],
                    ],
	                [
		                'route' => 'admin_sermon_download',
		                'route_parameters' => [
			                'id' => 'id'
		                ],
		                'icon' => 'glyphicon glyphicon-download-alt',
		                'attributes' => [
			                'rel' => 'tooltip',
			                'title' => $this->translator->trans('Download sermon'),
			                'class' => 'btn btn-primary btn-xs',
			                'role' => 'button'
		                ],
	                ],
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Sermon';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'sermon_datatable';
    }
}
