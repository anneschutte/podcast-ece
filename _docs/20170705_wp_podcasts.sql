/*
Navicat MySQL Data Transfer

Source Server         : Strato 2
Source Server Version : 50173
Source Host           : localhost:3306
Source Database       : eceuropoort_wp1

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2017-07-05 09:50:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wp_sb_books
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_books`;
CREATE TABLE `wp_sb_books` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_books
-- ----------------------------
INSERT INTO `wp_sb_books` VALUES ('1', 'Genesis');
INSERT INTO `wp_sb_books` VALUES ('2', 'Exodus');
INSERT INTO `wp_sb_books` VALUES ('3', 'Leviticus');
INSERT INTO `wp_sb_books` VALUES ('4', 'Numbers');
INSERT INTO `wp_sb_books` VALUES ('5', 'Deuteronomy');
INSERT INTO `wp_sb_books` VALUES ('6', 'Joshua');
INSERT INTO `wp_sb_books` VALUES ('7', 'Judges');
INSERT INTO `wp_sb_books` VALUES ('8', 'Ruth');
INSERT INTO `wp_sb_books` VALUES ('9', '1 Samuel');
INSERT INTO `wp_sb_books` VALUES ('10', '2 Samuel');
INSERT INTO `wp_sb_books` VALUES ('11', '1 Kings');
INSERT INTO `wp_sb_books` VALUES ('12', '2 Kings');
INSERT INTO `wp_sb_books` VALUES ('13', '1 Chronicles');
INSERT INTO `wp_sb_books` VALUES ('14', '2 Chronicles');
INSERT INTO `wp_sb_books` VALUES ('15', 'Ezra');
INSERT INTO `wp_sb_books` VALUES ('16', 'Nehemiah');
INSERT INTO `wp_sb_books` VALUES ('17', 'Esther');
INSERT INTO `wp_sb_books` VALUES ('18', 'Job');
INSERT INTO `wp_sb_books` VALUES ('19', 'Psalm');
INSERT INTO `wp_sb_books` VALUES ('20', 'Proverbs');
INSERT INTO `wp_sb_books` VALUES ('21', 'Ecclesiastes');
INSERT INTO `wp_sb_books` VALUES ('22', 'Song of Solomon');
INSERT INTO `wp_sb_books` VALUES ('23', 'Isaiah');
INSERT INTO `wp_sb_books` VALUES ('24', 'Jeremiah');
INSERT INTO `wp_sb_books` VALUES ('25', 'Lamentations');
INSERT INTO `wp_sb_books` VALUES ('26', 'Ezekiel');
INSERT INTO `wp_sb_books` VALUES ('27', 'Daniel');
INSERT INTO `wp_sb_books` VALUES ('28', 'Hosea');
INSERT INTO `wp_sb_books` VALUES ('29', 'Joel');
INSERT INTO `wp_sb_books` VALUES ('30', 'Amos');
INSERT INTO `wp_sb_books` VALUES ('31', 'Obadiah');
INSERT INTO `wp_sb_books` VALUES ('32', 'Jonah');
INSERT INTO `wp_sb_books` VALUES ('33', 'Micah');
INSERT INTO `wp_sb_books` VALUES ('34', 'Nahum');
INSERT INTO `wp_sb_books` VALUES ('35', 'Habakkuk');
INSERT INTO `wp_sb_books` VALUES ('36', 'Zephaniah');
INSERT INTO `wp_sb_books` VALUES ('37', 'Haggai');
INSERT INTO `wp_sb_books` VALUES ('38', 'Zechariah');
INSERT INTO `wp_sb_books` VALUES ('39', 'Malachi');
INSERT INTO `wp_sb_books` VALUES ('40', 'Matthew');
INSERT INTO `wp_sb_books` VALUES ('41', 'Mark');
INSERT INTO `wp_sb_books` VALUES ('42', 'Luke');
INSERT INTO `wp_sb_books` VALUES ('43', 'John');
INSERT INTO `wp_sb_books` VALUES ('44', 'Acts');
INSERT INTO `wp_sb_books` VALUES ('45', 'Romans');
INSERT INTO `wp_sb_books` VALUES ('46', '1 Corinthians');
INSERT INTO `wp_sb_books` VALUES ('47', '2 Corinthians');
INSERT INTO `wp_sb_books` VALUES ('48', 'Galatians');
INSERT INTO `wp_sb_books` VALUES ('49', 'Ephesians');
INSERT INTO `wp_sb_books` VALUES ('50', 'Philippians');
INSERT INTO `wp_sb_books` VALUES ('51', 'Colossians');
INSERT INTO `wp_sb_books` VALUES ('52', '1 Thessalonians');
INSERT INTO `wp_sb_books` VALUES ('53', '2 Thessalonians');
INSERT INTO `wp_sb_books` VALUES ('54', '1 Timothy');
INSERT INTO `wp_sb_books` VALUES ('55', '2 Timothy');
INSERT INTO `wp_sb_books` VALUES ('56', 'Titus');
INSERT INTO `wp_sb_books` VALUES ('57', 'Philemon');
INSERT INTO `wp_sb_books` VALUES ('58', 'Hebrews');
INSERT INTO `wp_sb_books` VALUES ('59', 'James');
INSERT INTO `wp_sb_books` VALUES ('60', '1 Peter');
INSERT INTO `wp_sb_books` VALUES ('61', '2 Peter');
INSERT INTO `wp_sb_books` VALUES ('62', '1 John');
INSERT INTO `wp_sb_books` VALUES ('63', '2 John');
INSERT INTO `wp_sb_books` VALUES ('64', '3 John');
INSERT INTO `wp_sb_books` VALUES ('65', 'Jude');
INSERT INTO `wp_sb_books` VALUES ('66', 'Revelation');

-- ----------------------------
-- Table structure for wp_sb_books_sermons
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_books_sermons`;
CREATE TABLE `wp_sb_books_sermons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(30) NOT NULL,
  `chapter` int(10) NOT NULL,
  `verse` int(10) NOT NULL,
  `order` int(10) NOT NULL,
  `type` varchar(30) DEFAULT NULL,
  `sermon_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sermon_id` (`sermon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_books_sermons
-- ----------------------------

-- ----------------------------
-- Table structure for wp_sb_preachers
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_preachers`;
CREATE TABLE `wp_sb_preachers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_preachers
-- ----------------------------
INSERT INTO `wp_sb_preachers` VALUES ('8', 'Gerard de Groot', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('7', 'Ruben De Jong', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('3', 'Daniel Renger', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('4', 'Willem Plaizier', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('5', 'Martin Koornstra', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('6', 'Daniel Pasterkamp', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('9', 'Ed van Setten', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('10', 'Remco de Zwart', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('11', 'David de Vos', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('12', ' Petra van Weerden', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('13', 'Jan Sjoerd Pasterkamp', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('14', 'Edgar Holder', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('15', 'Hugo van Leemputten', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('16', 'Steve van Deventer', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('17', 'Vanessa Steffens', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('18', 'Jan Pool', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('19', 'Kent Linneweh', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('20', 'Tanja van Schuylenburg', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('21', 'Willy van Iersel', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('22', 'Iwan Sahertian', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('23', 'Stuart Robinson', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('24', 'Astrid Feddes', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('25', 'Bruce Hills', '', '');
INSERT INTO `wp_sb_preachers` VALUES ('26', 'Wim Kok', '', '');

-- ----------------------------
-- Table structure for wp_sb_series
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_series`;
CREATE TABLE `wp_sb_series` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `page_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_series
-- ----------------------------
INSERT INTO `wp_sb_series` VALUES ('5', 'none', '0');
INSERT INTO `wp_sb_series` VALUES ('4', 'Jezus is...100% God', '0');
INSERT INTO `wp_sb_series` VALUES ('3', 'Jezus IS...100% mens', '0');
INSERT INTO `wp_sb_series` VALUES ('6', 'mercy, love and grace', '0');

-- ----------------------------
-- Table structure for wp_sb_sermons
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_sermons`;
CREATE TABLE `wp_sb_sermons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `preacher_id` int(10) NOT NULL,
  `datetime` datetime NOT NULL,
  `service_id` int(10) NOT NULL,
  `series_id` int(10) NOT NULL,
  `start` text NOT NULL,
  `end` text NOT NULL,
  `description` text,
  `time` varchar(5) DEFAULT NULL,
  `override` tinyint(1) DEFAULT NULL,
  `page_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_sermons
-- ----------------------------
INSERT INTO `wp_sb_sermons` VALUES ('1', 'Jezus is...de auteur van de grondwet van Gods Koninkrijk', '3', '2015-12-06 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('2', 'Laat Uw koninkrijk komen', '4', '2015-11-29 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('3', 'Jezus groeide in gunst van God en van mensen! En jij?', '3', '2015-11-15 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('4', 'Blijf staan in de storm', '5', '2015-11-08 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('5', 'Jezus...is vrede', '6', '2015-11-01 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('6', 'Sleutels om Gods stem te verstaan', '3', '2015-10-25 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('7', 'Jezus deelt Zijn geheimen', '7', '2015-10-18 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('8', 'Jezus is...de vreemdeling', '3', '2015-10-11 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('9', 'Jezus is...geen timmerman, maar een huizenbouwer', '6', '2015-10-04 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('10', 'Jezus is...de bouwer van Zijn gemeente', '3', '2015-09-06 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('11', 'Jezus is...100% mens', '4', '2015-09-20 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('12', 'Jezus is...wedergeboorte', '6', '2015-09-27 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('13', 'Jezus is...Ik ben', '3', '2015-09-13 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('14', 'Jezus is...gewichtloos', '6', '2015-12-13 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('15', 'Dankbaarheid, de taal van kinderen van Gods Koninkrijk', '3', '2015-12-27 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('16', 'Jezus is...op zoek naar gastvrijheid', '6', '2016-01-03 10:00:00', '5', '3', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('17', 'Ontvang je hemelse upgrade', '8', '2016-01-10 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('21', 'Wie is de Vader van jouw leven?', '9', '2016-01-24 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('20', 'Jezus is...de opstanding', '10', '2016-01-31 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('22', 'Jezus, wie is Hij toch?', '4', '2016-02-07 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('23', 'Maak je plannen vol geloof bekend aan God', '11', '2016-02-14 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('24', '‘Ben jij ook overtuigd? Van kennis naar overtuiging dat Jezus de waarheid is’.', '12', '2016-02-21 10:00:00', '5', '5', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('25', 'Jezus is Koning van het ondersteboven Koninkrijk', '3', '2016-02-28 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('26', 'De kracht van goede keuzes', '13', '2016-03-06 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('27', 'Geroepen tot Gods volheid', '14', '2016-03-13 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('33', 'Dwaas of reddende kracht?', '7', '2016-03-27 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('34', 'Hart voor het Huis: Jezus is op zoek naar 1', '6', '2016-04-03 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('32', 'Brandhout:Vaders grondstof voor een doorbraak', '15', '2016-03-20 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('35', 'Geloof als taal van het nieuwe leven', '3', '2016-04-10 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('36', 'Making the Impossible Possible', '16', '2016-04-17 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('37', 'Jezus is...broodnodig', '6', '2016-04-24 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('38', 'Jezus leeft relatie', '7', '2016-05-01 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('39', 'De Heere is mijn Herder', '17', '2016-05-08 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('40', 'De  doop in de Heilige Geest', '6', '2016-05-15 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('41', 'Gezond Gezin Zijn', '3', '2016-05-22 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('45', 'Intimiteit met God', '18', '2016-05-29 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('43', 'Jezus is..Het Koninkrijk Nabij', '6', '2016-06-05 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('46', 'Intimiteit met God', '18', '1970-01-01 00:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('47', 'geef nooit op', '3', '2016-06-12 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('48', '', '6', '2016-06-19 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('49', '', '5', '2016-06-26 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('50', 'de 14 dagen \\\"speak-life\\\" challenge', '19', '2016-07-10 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('51', 'overdrachtsdienst daniel', '3', '2016-07-03 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('52', 'hart van God \\\"gerechtigheid\\\"', '20', '2016-07-17 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('53', '14 days challenge part 2', '19', '2016-07-24 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('54', 'het evangelie', '9', '2016-07-31 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('55', 'de schuilplaats', '21', '2016-08-07 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('56', 'Ontmoeting met God', '7', '2016-08-14 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('57', 'but God remembered', '19', '2016-08-21 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('58', 'God heeft ons uitgekozen', '6', '2016-08-28 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('59', 'Mercy-Love and Grace', '6', '2016-09-04 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('60', 'Visie zondag', '6', '2016-09-11 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('61', 'wat is in jouw hand ?', '6', '2016-09-18 10:00:00', '5', '4', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('62', 'Aanbidding is...', '22', '2016-09-25 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('63', 'Gebed', '6', '2016-10-02 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('64', 'knock knock who\\\'s there', '19', '2016-10-09 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('65', 'pasgeborene', '6', '2016-10-16 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('66', 'de beloofde hand', '7', '2016-10-23 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('67', 'De tegenwoordigheid van God binnen gaan', '6', '2016-10-30 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('68', 'David & Goliath day 41', '23', '2016-11-06 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('69', 'barmhartigheid en liefde van God', '6', '2016-11-13 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('70', 'dealen met tegenslagen en blijven kijken op Jezus', '5', '2016-11-20 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('71', 'Benjamin', '7', '2016-11-27 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('72', 'familie zijn', '6', '2016-12-04 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('73', 'compassion', '10', '2016-12-11 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('74', 'Gerechtigheid', '6', '2016-12-18 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('75', 'God wil jouw kennen', '6', '2016-12-25 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('76', 'vergeving', '24', '2017-01-08 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('77', 'Gods plan voor komend jaar', '6', '2017-01-01 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('78', 'God\\\'s new thing', '25', '2017-01-15 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('79', 'De doop en wat het betekend', '6', '2017-01-22 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('80', 'vergeving', '3', '2017-01-29 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('81', 'de kracht van vergeving', '6', '2017-02-05 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('82', 'het geven van je financiën', '8', '2017-02-12 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('83', 'zegen door financiën', '6', '2017-02-19 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('84', 'Liefde', '7', '2017-02-26 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('85', 'waarheid en genade zijn gekomen door Jezus', '6', '2017-03-05 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('86', 'Trouw aan God\\\'s liefde', '6', '2017-03-12 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('87', 'Family by design', '19', '2017-03-19 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('88', 'Vijf diamanten', '3', '2017-03-26 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('89', 'Familie zijn', '6', '2017-04-02 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('90', 'De doop als demonstratie', '7', '2017-04-09 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('91', 'omdat Jezus is opgestaan', '6', '2017-04-16 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('92', 'Genade', '13', '2017-04-23 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('93', 'Profetie', '26', '2017-04-30 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('94', 'Wat is genade', '6', '2017-05-07 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('95', 'Moederdag', '11', '2017-05-14 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('96', 'Wat is genade deel 2', '6', '2017-05-21 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('97', 'Wat is mijn indentiteit', '18', '2017-05-28 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('98', 'de geest van genade uigestort met pinkesteren', '6', '2017-06-04 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('99', 'Hemelse kracht die doorbreekt op aarde!', '5', '2017-06-11 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');
INSERT INTO `wp_sb_sermons` VALUES ('100', 'Weerstand tegen verleiding', '6', '2017-06-18 10:00:00', '5', '6', 'a:0:{}', 'a:0:{}', '', '', '0', '0');

-- ----------------------------
-- Table structure for wp_sb_sermons_tags
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_sermons_tags`;
CREATE TABLE `wp_sb_sermons_tags` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sermon_id` int(10) NOT NULL,
  `tag_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sermon_id` (`sermon_id`)
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_sermons_tags
-- ----------------------------
INSERT INTO `wp_sb_sermons_tags` VALUES ('27', '1', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('9', '2', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('10', '3', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('11', '4', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('28', '5', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('13', '6', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('18', '7', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('15', '8', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('16', '9', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('23', '10', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('20', '11', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('21', '12', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('22', '13', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('24', '14', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('25', '15', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('26', '16', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('29', '17', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('53', '33', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('36', '21', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('33', '20', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('37', '22', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('38', '23', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('39', '24', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('40', '25', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('42', '26', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('43', '27', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('49', '32', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('51', '34', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('55', '35', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('57', '36', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('56', '37', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('58', '38', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('59', '39', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('60', '40', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('61', '41', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('69', '45', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('72', '43', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('70', '46', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('71', '47', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('74', '48', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('75', '49', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('76', '50', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('77', '51', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('78', '52', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('79', '53', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('80', '54', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('81', '55', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('82', '56', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('83', '57', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('85', '58', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('86', '59', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('87', '60', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('88', '61', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('89', '62', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('91', '63', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('92', '64', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('94', '65', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('95', '66', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('96', '67', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('97', '68', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('98', '69', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('100', '70', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('101', '71', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('102', '72', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('103', '73', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('104', '74', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('105', '75', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('106', '76', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('107', '77', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('108', '78', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('109', '79', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('110', '80', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('111', '81', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('113', '82', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('114', '83', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('115', '84', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('116', '85', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('117', '86', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('118', '87', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('119', '88', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('120', '89', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('122', '90', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('123', '91', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('124', '92', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('125', '93', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('129', '94', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('127', '95', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('128', '96', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('130', '97', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('131', '98', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('132', '99', '1');
INSERT INTO `wp_sb_sermons_tags` VALUES ('133', '100', '1');

-- ----------------------------
-- Table structure for wp_sb_services
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_services`;
CREATE TABLE `wp_sb_services` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `time` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_services
-- ----------------------------
INSERT INTO `wp_sb_services` VALUES ('5', 'Zondagochtend', '10:00');

-- ----------------------------
-- Table structure for wp_sb_stuff
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_stuff`;
CREATE TABLE `wp_sb_stuff` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `name` text NOT NULL,
  `sermon_id` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `duration` varchar(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_stuff
-- ----------------------------
INSERT INTO `wp_sb_stuff` VALUES ('1', 'file', '20151206Daniel Renger_Jezus is de auteur van de grondwet van Gods Koninkrijk.mp3', '1', '959', '51:58');
INSERT INTO `wp_sb_stuff` VALUES ('2', 'file', '20151129Willem Plaizier_Laat Uw koninkrijk komen.mp3', '2', '899', '37:08');
INSERT INTO `wp_sb_stuff` VALUES ('3', 'file', '20151115Daniel Renger_Jezus groeide in gunst van God en van mensen En jij.mp3', '3', '907', '43:56');
INSERT INTO `wp_sb_stuff` VALUES ('4', 'file', '20151108Martin Koornstra_Blijf staan in de storm.mp3', '4', '919', '34:02');
INSERT INTO `wp_sb_stuff` VALUES ('5', 'file', '20151101Daniel Pasterkamp_Jezus is vrede.mp3', '5', '626', '51:12');
INSERT INTO `wp_sb_stuff` VALUES ('6', 'file', '20151025Daniel Renger_Sleutels om Gods stem te verstaan.mp3', '6', '1180', '43:32');
INSERT INTO `wp_sb_stuff` VALUES ('7', 'file', '20151018Ruben de Jong.mp3', '7', '577', '48:09');
INSERT INTO `wp_sb_stuff` VALUES ('8', 'file', '20151011Daniel Renger_Jezus is de vreemdeling.mp3', '8', '541', '49:10');
INSERT INTO `wp_sb_stuff` VALUES ('9', 'file', '20151004Daniel Pasterkamp_Jezus is.mp3', '9', '492', '40:33');
INSERT INTO `wp_sb_stuff` VALUES ('10', 'file', '20150927Daniel Pasterkamp_JEZUS IS wedergeboorte.mp3', '12', '454', '39:00');
INSERT INTO `wp_sb_stuff` VALUES ('11', 'file', '20150920Willem Plaizier_Jezus 100% mens.mp3', '11', '451', '47:06');
INSERT INTO `wp_sb_stuff` VALUES ('12', 'file', '20150913Daniel Renger_Ik ben.mp3', '13', '433', '33:59');
INSERT INTO `wp_sb_stuff` VALUES ('13', 'file', '20150906Daniel Renger_Startzondag.mp3', '10', '373', '43:43');
INSERT INTO `wp_sb_stuff` VALUES ('14', 'file', '20151213Daniel Pasterkamp_Jezus is gewichtloos.mp3', '14', '959', '31:09');
INSERT INTO `wp_sb_stuff` VALUES ('15', 'file', '20151227Daniel Renger_Dankbaarheid, de taal van kinderen van Gods Koninkrijk.mp3', '15', '1105', '31:45');
INSERT INTO `wp_sb_stuff` VALUES ('16', 'file', '20160103Daniel Pasterkamp_Jezus is op zoek naar gastvrijheid.mp3', '16', '1035', '24:06');
INSERT INTO `wp_sb_stuff` VALUES ('17', 'file', '20160110Gerard de Groot_Ontvang je hemelse upgrade.mp3', '17', '1142', '43:05');
INSERT INTO `wp_sb_stuff` VALUES ('20', 'file', '20160124Ed van Setten_Wie is de vader van jouw leven.mp3', '21', '820', '50:30');
INSERT INTO `wp_sb_stuff` VALUES ('19', 'file', '20160131Remco de Zwart_Jezus is de opstanding.mp3', '20', '955', '38:47');
INSERT INTO `wp_sb_stuff` VALUES ('21', 'file', '20160207Willem Plaizier_Jezus wie is hij toch.mp3', '22', '876', '35:27');
INSERT INTO `wp_sb_stuff` VALUES ('22', 'file', '20160214David de Vos_Maak je plannen vol geloof bekend aan God.mp3', '23', '1606', '49:31');
INSERT INTO `wp_sb_stuff` VALUES ('23', 'file', 'preek petra.m4a', '24', '1226', '0');
INSERT INTO `wp_sb_stuff` VALUES ('24', 'file', 'preek daniel 28-2-2016.m4a', '25', '1297', '0');
INSERT INTO `wp_sb_stuff` VALUES ('25', 'file', 'preek jan sjoerd pasterkamp.m4a', '26', '5587', '0');
INSERT INTO `wp_sb_stuff` VALUES ('26', 'file', '13-3-2016 preek edgar holder.m4a', '27', '1530', '0');
INSERT INTO `wp_sb_stuff` VALUES ('29', 'file', 'Audio Hugo-ECE-20-3-2016.mp3', '32', '896', '58:11');
INSERT INTO `wp_sb_stuff` VALUES ('30', 'file', 'preek ruben de jong 27-3-2016.m4a', '33', '850', '0');
INSERT INTO `wp_sb_stuff` VALUES ('31', 'file', 'preek daniel pasterkamp 3-4-2016.m4a', '34', '61478', '0');
INSERT INTO `wp_sb_stuff` VALUES ('32', 'file', 'preek daniel renger 10-4-2016.m4a', '35', '914', '0');
INSERT INTO `wp_sb_stuff` VALUES ('33', 'file', 'preek Steve van Deventer 17-4-2016.m4a', '36', '1118', '0');
INSERT INTO `wp_sb_stuff` VALUES ('34', 'file', 'preek daniel pasterkamp 24-4-2016.m4a', '37', '983', '0');
INSERT INTO `wp_sb_stuff` VALUES ('35', 'file', 'preek Ruben de Jong 1-5-2016.m4a', '38', '1101', '0');
INSERT INTO `wp_sb_stuff` VALUES ('36', 'file', 'preek Vanessa Steffens 8-5-2016.m4a', '39', '62268', '0');
INSERT INTO `wp_sb_stuff` VALUES ('37', 'file', 'preek daniel pasterkamp zondag 15-5-2016.m4a', '40', '919', '0');
INSERT INTO `wp_sb_stuff` VALUES ('38', 'file', 'preek daniel renger zondag 22-5-2016.m4a', '41', '1186', '0');
INSERT INTO `wp_sb_stuff` VALUES ('39', 'file', 'preek Jan Pool zondag 29-5-2016.m4a', '0', '65762', '0');
INSERT INTO `wp_sb_stuff` VALUES ('40', 'file', 'preek daniel pasterkamp zondag 5-6-2016.m4a', '0', '40', '0');
INSERT INTO `wp_sb_stuff` VALUES ('41', 'file', 'preek daniel pasterkamp zondag 5-6-2016_1.m4a', '43', '637', '0');
INSERT INTO `wp_sb_stuff` VALUES ('42', 'file', 'Jan Pool zondag 29-5-2016.m4a', '46', '304', '0');
INSERT INTO `wp_sb_stuff` VALUES ('43', 'file', 'preek daniel renger zondag 12-6-2016.m4a', '47', '775', '0');
INSERT INTO `wp_sb_stuff` VALUES ('44', 'file', 'preek daniel pasterkamp 12-6-2016.m4a', '0', '0', '0');
INSERT INTO `wp_sb_stuff` VALUES ('45', 'file', 'preek daniel pasterkamp 19-6-2016.m4a', '48', '635', '0');
INSERT INTO `wp_sb_stuff` VALUES ('46', 'file', 'preek  zondag 26-6-2016 martin koornstra.m4a', '49', '884', '0');
INSERT INTO `wp_sb_stuff` VALUES ('47', 'file', 'preek 10-7-2016 Kent Linneweh -de 14 dagen  speaklife challenge.m4a', '50', '689', '0');
INSERT INTO `wp_sb_stuff` VALUES ('48', 'file', 'overdrachtsdienst 3-7-2016.m4a', '51', '601', '0');
INSERT INTO `wp_sb_stuff` VALUES ('49', 'file', 'preek 17-7-2016 Tanja schuylenburg hart van God gerechtigheid.m4a', '52', '564', '0');
INSERT INTO `wp_sb_stuff` VALUES ('50', 'file', 'preek 24-7-2016 kent linneweh 14 day challenge part 2.m4a', '53', '481', '0');
INSERT INTO `wp_sb_stuff` VALUES ('51', 'file', 'preek 31-7-2016 ed van setten het evangelie.m4a', '54', '521', '0');
INSERT INTO `wp_sb_stuff` VALUES ('52', 'file', 'preek 7-8-2016 willy van Iersel de schuilplaats.m4a', '55', '513', '0');
INSERT INTO `wp_sb_stuff` VALUES ('53', 'file', 'preek 14-8-2016 Ruben de Jong ontmoeting met God.m4a', '56', '488', '0');
INSERT INTO `wp_sb_stuff` VALUES ('54', 'file', 'preek 21-8-2016 Kent Linneweh but God remembered.m4a', '57', '452', '0');
INSERT INTO `wp_sb_stuff` VALUES ('55', 'file', 'preek 28-8-2016 daniel pasterkamp God heeft ons uitgekozen.m4a', '58', '488', '0');
INSERT INTO `wp_sb_stuff` VALUES ('56', 'file', 'preek 4-9-2016 daniel pasterkamp  Mercy-Love and Grace.m4a', '59', '477', '0');
INSERT INTO `wp_sb_stuff` VALUES ('57', 'file', 'preek 11-9-2016 daniel pasterkamp  Visie zondag.m4a', '60', '488', '0');
INSERT INTO `wp_sb_stuff` VALUES ('58', 'file', 'preek 18-9-2016 daniel pasterkamp  Wat is in jouw hand _.m4a', '61', '581', '0');
INSERT INTO `wp_sb_stuff` VALUES ('59', 'file', 'preek 25-9-2016 Iwan Sahertian  Aanbidding is....m4a', '62', '548', '0');
INSERT INTO `wp_sb_stuff` VALUES ('60', 'file', 'preek 2-10-2016 daniel pasterkamp  Gebed.m4a', '63', '443', '0');
INSERT INTO `wp_sb_stuff` VALUES ('61', 'file', 'preek 9-10-2016 kent linneweh knock knock who\'s there.m4a', '64', '402', '0');
INSERT INTO `wp_sb_stuff` VALUES ('62', 'file', 'preek 16-10-2016 daniel pasterkamp pasgeborene.m4a', '65', '435', '0');
INSERT INTO `wp_sb_stuff` VALUES ('63', 'file', 'preek 23-10-2016 Ruben de Jong; de beloofde hand.m4a', '66', '376', '0');
INSERT INTO `wp_sb_stuff` VALUES ('64', 'file', 'preek 30-10-2016 Daniel pasterkamp  De tegenwoordigheid van God binnen gaan.m4a', '67', '444', '0');
INSERT INTO `wp_sb_stuff` VALUES ('65', 'file', 'preek 6-11-2016 David & Goliath day 41.m4a', '68', '444', '0');
INSERT INTO `wp_sb_stuff` VALUES ('66', 'file', 'preek 13-11-2016 daniel pastekamp barmhartigheid en liefde van God.m4a', '69', '260', '0');
INSERT INTO `wp_sb_stuff` VALUES ('67', 'file', 'preek 20-11-2016 martin koornstra dealen met tegenslagen en blijven kijken op Jezus.m4a', '70', '304', '0');
INSERT INTO `wp_sb_stuff` VALUES ('68', 'file', 'preek 27-11-2016 Ruben de Jong  Benjamin.m4a', '71', '287', '0');
INSERT INTO `wp_sb_stuff` VALUES ('69', 'file', 'preek 04-12-2016 daniel pasterkamp famile zijn.m4a', '72', '259', '0');
INSERT INTO `wp_sb_stuff` VALUES ('70', 'file', 'preek 11-12-2016 Rmeco de Zwart  Compassion.m4a', '73', '280', '0');
INSERT INTO `wp_sb_stuff` VALUES ('71', 'file', 'preek 18-12-2016 daniel Pasterkamp  gerechtigheid.m4a', '74', '282', '0');
INSERT INTO `wp_sb_stuff` VALUES ('72', 'file', 'preek 25-12-2016 daniel Pasterkamp  God wil jouw kennen.m4a', '75', '292', '0');
INSERT INTO `wp_sb_stuff` VALUES ('73', 'file', 'preek 8-1-2017 Astrid Feddes  Vergeving.m4a', '76', '342', '0');
INSERT INTO `wp_sb_stuff` VALUES ('74', 'file', 'preek 1-1-2017 daniel pasterkamp Gods plan voor komend jaar.m4a', '77', '297', '0');
INSERT INTO `wp_sb_stuff` VALUES ('75', 'file', 'preek 15-1-2017 Bruce Hills - God\'s new thing.m4a', '78', '356', '0');
INSERT INTO `wp_sb_stuff` VALUES ('76', 'file', 'preek 22-1-2017 Daniel Pasterkamp  De doop en wat het betekend.m4a', '79', '283', '0');
INSERT INTO `wp_sb_stuff` VALUES ('77', 'file', 'preek 29-1 -2017 Daniel Renger Vergeving.m4a', '80', '358', '0');
INSERT INTO `wp_sb_stuff` VALUES ('78', 'file', 'preek 5-2-2017 daniel pasterkamp de kracht van vergeving.m4a', '81', '336', '0');
INSERT INTO `wp_sb_stuff` VALUES ('79', 'file', 'preek 12-2-2017 Gerard de Groot het geven van financiën.m4a', '82', '309', '0');
INSERT INTO `wp_sb_stuff` VALUES ('80', 'file', 'preek 19-2-2017 daniel pasterkamp zegen door financien.m4a', '83', '248', '0');
INSERT INTO `wp_sb_stuff` VALUES ('81', 'file', 'preek 26-2-2017 Ruben de Jong  Liefde.m4a', '84', '209', '0');
INSERT INTO `wp_sb_stuff` VALUES ('82', 'file', 'preek 5-3-2017 Daniel Pasterkamp Waarheid en genade zijn gekomen door Jezus.m4a', '85', '158', '0');
INSERT INTO `wp_sb_stuff` VALUES ('83', 'file', 'preek 12-3-2017 Daniel Pasterkamp Trouw aan God\'s liefde.m4a', '86', '136', '0');
INSERT INTO `wp_sb_stuff` VALUES ('84', 'file', 'preek 19-3-2017 Kent Linneweh Family by design.m4a', '87', '145', '0');
INSERT INTO `wp_sb_stuff` VALUES ('85', 'file', 'preek 26-3-2017 Daniel Renger Vijf diamanten.m4a', '88', '221', '0');
INSERT INTO `wp_sb_stuff` VALUES ('86', 'file', 'preek 2-4-2017 daniel pasterkamp Familie zijn.m4a', '89', '135', '0');
INSERT INTO `wp_sb_stuff` VALUES ('87', 'file', 'preek 9-4-2017 Ruben de Jong De doop als demonstratie.m4a', '90', '96', '0');
INSERT INTO `wp_sb_stuff` VALUES ('88', 'file', 'preek 16-4-2017 daniel pasterkamp Omdat Jezus is opgestaan.m4a', '91', '78', '0');
INSERT INTO `wp_sb_stuff` VALUES ('89', 'file', 'preek 23-4-2017 Jan sjoerd pasterkamp Genade.m4a', '92', '148', '0');
INSERT INTO `wp_sb_stuff` VALUES ('90', 'file', 'preek 30-4-2017 Wim en Corrie kok profetie.m4a', '93', '123', '0');
INSERT INTO `wp_sb_stuff` VALUES ('91', 'file', 'preek 7-5-2017 daniel pasterkamp genade.m4a', '94', '99', '0');
INSERT INTO `wp_sb_stuff` VALUES ('92', 'file', 'preek 14-5-2017 David de Vos Moederdag.m4a', '95', '127', '0');
INSERT INTO `wp_sb_stuff` VALUES ('93', 'file', 'preek 21-5-2017 daniel Pasterkamp Wat is genade deel 2.m4a', '96', '76', '0');
INSERT INTO `wp_sb_stuff` VALUES ('94', 'file', 'preek 28-5-2017 Jan Pool Wat is mijn Indentiteit.m4a', '97', '202', '0');
INSERT INTO `wp_sb_stuff` VALUES ('95', 'file', 'preek 4-6-2017 daniel pasterkamp de geest van genade uigestort met pinkesteren.m4a', '98', '57', '0');
INSERT INTO `wp_sb_stuff` VALUES ('96', 'file', 'dienst 11-6-2017 - Martin Koornstra - Hemelse kracht die doorbreekt op aarde!.m4a', '99', '180', '0');
INSERT INTO `wp_sb_stuff` VALUES ('97', 'file', 'preek 18-6-2017 - daniel pasterkamp Weerstand tegen verleiding.m4a', '100', '65', '0');

-- ----------------------------
-- Table structure for wp_sb_tags
-- ----------------------------
DROP TABLE IF EXISTS `wp_sb_tags`;
CREATE TABLE `wp_sb_tags` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wp_sb_tags
-- ----------------------------
INSERT INTO `wp_sb_tags` VALUES ('1', '');
SET FOREIGN_KEY_CHECKS=1;
